## About us

Activism.International is an informal association of activists with technical knowledge who want to share their abilities to everyone.

Mostly but not only, we consist of

- *People from the climate justice movement*
- [Extinction Rebellion Hamburg-West](mailto:hh-west@extinctionrebellion.de)

Please note that most of our contributors do *not* want to be listed here.

Our servers are sponsored by [TestApp.schule](https://testapp.schule).

## Legal notice

Legal notice according to German § 5 TMG:

Maintainer of this website and the whole online services [listed on our home page](/) is:

!!! info
    ```
    Activism.International maintainers
    August-Kirch-Str. 15j
    D-22525 Hamburg
    GERMANY / European Union
    ```

- Mail: info@activism.international *[[PGP](https://keys.mailvelope.com/pks/lookup?op=get&search=info@activism.international)]*
- Phone: +49 221 59619 2183

[Our privacy notices](privacy.md)

[Donate 💚](https://www.buymeacoffee.com/testapp){: .md-button .md-button--primary }
