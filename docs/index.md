![Logo featuring a fist inside clouds](assets/logo.svg){: loading=lazy align=left width=128 }

# Climate Justice. Let's shut shit down.

## The Activism Cloud


!!! info
    ***You are looking for a unified way to organize the revolution? We provide the Activism Cloud.***

    The [Activism Cloud](https://cloud.activism.international/) provides access to numerous services with one single login, your Activism-ID. In the Activism Cloud, you can write encrypted messages - even to people who do not use Activism Cloud - , store files, create calendars and plan video conferences etc.

    Everyone can register, create a local group and collaborate in real-time. Using modern online office, you can take advanced notes, edit office documents, plan presentations, or share media files.

You can find a quick introduction into the Activism Cloud [here](https://cloud.activism.international/apps/onlyoffice/s/9bzbQgwBjy9e2Za).

## What is Activism.International ?

Activism.International is a supporter of every eco activism. We provide online tools for secure communication, conferences etc. for free.

Our tools include:

| Tool                                                       |                                          Cloud-App                                           | What it does                                                                                                  | Learn more                                  |                                   Privacy                                    |        Account (required)        |
| :--------------------------------------------------------- | :------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------ | :------------------------------------------ | :--------------------------------------------------------------------------: | :------------------------------: |
| [Activism Cloud](https://cloud.activism.international/)    |                      [Dashboard](https://cloud.activism.international/)                      | Encrypted files, shared calendars and collaborative writing, integrates all our services at one unified place | [Nextcloud](https://nextcloud.com/)         |                       [Cloud policy](privacy/cloud.md)                       |         :material-check:         |
| [Matrix (Element)](https://matrix.activism.international/) |                [Element](https://cloud.activism.international/apps/riotchat)                 | Encrypted messaging and internet telephony                                                                    | [Matrix](https://matrix.org/)               |                       [Cloud policy](privacy/cloud.md)                       |         :material-check:         |
| [BigBlueButton](https://bbb.activism.international/)       |                [BigBlueButton](https://cloud.activism.international/apps/bbb)                | Video conferences with many participants, landline dial-in, slides and many extras                            | [BigBlueButton](https://bigbluebutton.org/) | [Cloud policy](privacy/cloud.md) or [Temporary policy](privacy/temporary.md) |             optional             |
| [Jitsi](https://meet.activism.international/)              |                                       :material-close:                                       | Simple and quick video chat                                                                                   | [Jitsi](https://meet.jit.si/)               |                   [Temporary policy](privacy/temporary.md)                   |         :material-close:         |
| [Pad](https://pad.activism.international/)                 | (create a Text Document [inside the Cloud](https://cloud.activism.international/apps/files)) | Shared notes                                                                                                  | [Etherpad](https://etherpad.org/)           | [Cloud policy](privacy/cloud.md) or [Temporary policy](privacy/temporary.md) | only when using inside the Cloud |

## Getting started

You can simply register [here](https://cloud.activism.international/apps/registration/). Once registered, you can access all our services and enjoy a friendly environment for revolutionary organization.

**We do *not* ask you for a mail address, phone number or any other personal data during registration.**

If you have any questions or miss some features, feel free to [contact us](mailto:info@activism.international) or consult the [beginner's guide](https://cloud.activism.international/apps/onlyoffice/s/9bzbQgwBjy9e2Za).

## How much may I use Activism Cloud ?

Any eco activist group complying with our [usage policy](usage-policy.md) may use Activism Cloud, even for private stuff. Per user, we initially provide 10 GB of online storage. If this is insufficient, you may contact us for more storage — we are solidary.

If you intensively use Activism Cloud, please consider supporting the solidary tech group by contributing with your local tech people (no need for deep knowledge or experience) or by [donating for our servers](about.md).

## Something is missing ?

Contact us [info@activism.international](mailto:info@activism.international).

## Security

Our servers are located in Hamburg / Germany and run on a Proxmox data center. All software we use is open source.

[Learn more on our security mechanisms](security.md)

## Who we are

Activism.International is provided by the solidary tech group, an informal association of activists with technical knowledge who want to share their abilities to everyone.

[Learn more](about.md)
